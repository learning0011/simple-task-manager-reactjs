import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import PropTypes from "prop-types"; // Import prop-types
import { useState } from "react";

function DateCreator({ onChange }) {
  const [date, setDate] = useState(new Date());

  const handleDateChange = (newDate) => {
    setDate(newDate);
    onChange(newDate); // Call the passed onChange function with the new date
  };

  return (
    <div>
      <DatePicker
        showTimeSelect
        minTime={new Date(0, 0, 0, 12, 30)}
        maxTime={new Date(0, 0, 0, 19, 0)}
        selected={date}
        onChange={handleDateChange}
        dateFormat="MMMM d, yyyy h:mmaa"
      />
    </div>
  );
}

// Define prop types for DateCreator
DateCreator.propTypes = {
  // date: PropTypes.instanceOf(Date).isRequired, // `date` must be a Date object
  onChange: PropTypes.func.isRequired, // `setDate` must be a function
};

export default DateCreator;
