import { useState } from "react";
import PropTypes from "prop-types"; // Import prop-types
import styles from "./form.module.css";

function Form({ tasks, setTasks }) {
  // const [task, setTask] = useState("");
  const [task, setTask] = useState({ name: "", done: false });

  const handleSubmit = (e) => {
    e.preventDefault(); // This will prevent the form from submitting
    if (task.name.length === 0) return;

    setTasks([...tasks, task]);
    setTask({ name: "", done: false });
    console.log(tasks); // Since callback tasks are not updated yet
  };

  return (
    <form onSubmit={handleSubmit} className={styles.todoForm}>
      {/* <DateCreator onChange={handleDateChange}></DateCreator> */}
      <div className={styles.inputContainer}>
        <input
          className={styles.modernInput}
          type="text"
          placeholder="Enter Task"
          value={task.name}
          onChange={(e) => {
            setTask({ name: e.target.value, done: false });
          }}
        ></input>
        <button className={styles.modernButton}>Add</button>
      </div>
    </form>
  );
}

Form.propTypes = {
  tasks: PropTypes.array.isRequired, // `setDate` must be a function
  setTasks: PropTypes.func.isRequired,
};

export default Form;
