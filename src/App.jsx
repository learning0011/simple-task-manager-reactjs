import Task from "./Components/Task";
import Header from "./Components/Header";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Task></Task>
    </div>
  );
}

export default App;
