import Footer from "./Footer";
import Form from "./Form";
import TaskList from "./TaskList";
import { useState } from "react";

// import DateCreator from "./DateCreator";

let Task = () => {
  // {
  // date: new Date(),
  // taskName: "",
  // }

  // const handleDateChange = (date) => {
  //   setTask({ ...task, date: date });
  // };
  const [tasks, setTasks] = useState([]);
  const completedTaskCount = tasks.filter((task) => task.done === true).length;
  const totalTaskCount = tasks.length;
  return (
    <div>
      <Form tasks={tasks} setTasks={setTasks}></Form>
      <TaskList tasks={tasks} setTasks={setTasks}></TaskList>
      <Footer
        completedTaskCount={completedTaskCount}
        totalTaskCount={totalTaskCount}
      ></Footer>
    </div>
  );
};

export default Task;
