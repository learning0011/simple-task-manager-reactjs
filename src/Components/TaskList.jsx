import TaskItem from "./TaskItem";
import PropTypes from "prop-types"; // Import prop-types
import styles from "./taskList.module.css";

function TaskList({ tasks, setTasks }) {
  if (tasks.length !== 0) {
    const sortedTasks = tasks
      .slice()
      .sort((a, b) => Number(a.done) - Number(b.done));
    return (
      <div className={styles.list}>
        {sortedTasks.map((item) => (
          <TaskItem
            key={tasks.indexOf(item)}
            item={item}
            tasks={tasks}
            setTasks={setTasks}
          ></TaskItem>
        ))}
      </div>
    );
  } else {
    return (
      <div>
        <p> No Tasks Found </p>
      </div>
    );
  }
}

TaskList.propTypes = {
  tasks: PropTypes.array.isRequired, // `setDate` must be a function
  setTasks: PropTypes.func.isRequired,
};

export default TaskList;
