import PropTypes from "prop-types"; // Import prop-types
import styles from "./taskItem.module.css";

function TaskItem({ item, tasks, setTasks }) {
  function handleDelete() {
    console.log(item, "Deleted");
    setTasks(tasks.filter((task) => task !== item));
  }

  function handleClick(name) {
    console.log(name, "Clicked");

    setTasks(
      tasks.map((task) =>
        task.name === name ? { ...task, done: !task.done } : task
      )
    );
  }

  const comletedStyleName = item.done ? styles.completed : styles.taskButton;

  return (
    <div className={styles.item}>
      <div className={styles.itemName}>
        <span
          className={comletedStyleName}
          onClick={() => {
            handleClick(item.name);
          }}
        >
          {item.name}
        </span>

        <span>
          <button onClick={handleDelete} className={styles.deleteButton}>
            x
          </button>
        </span>
      </div>
      <hr className={styles.line} />
    </div>
  );
}

TaskItem.propTypes = {
  item: PropTypes.object.isRequired, // `setDate` must be a function
  tasks: PropTypes.array.isRequired, // `setDate` must be a function
  setTasks: PropTypes.func.isRequired, // `setDate` must be a function
};
export default TaskItem;
