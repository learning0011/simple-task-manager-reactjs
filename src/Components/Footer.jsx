import PropTypes from "prop-types"; // Import prop-types
import styles from "./footer.module.css";

function Footer({ completedTaskCount, totalTaskCount }) {
  return (
    <div className={styles.footer}>
      <span className={styles.item}>Completed Task : {completedTaskCount}</span>
      <span className={styles.item}>Total Task : {totalTaskCount}</span>
    </div>
  );
}

Footer.propTypes = {
  completedTaskCount: PropTypes.number.isRequired,
  totalTaskCount: PropTypes.number.isRequired,
};

export default Footer;
